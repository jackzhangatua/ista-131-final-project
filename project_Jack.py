"""
This module studies the industry for Data Scientist. For example, which companies hire Data Scientists; how many years
do people usually stay in the companies; what are the compensations for the experienced and non-experienced Data
Scientists, etc.
Author: Jack Zhang
"""

from collections import Counter

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from Plotting_Assistant import plot_regression_line, format_plot

from data_preprocessing import preprocess


def plot_yii_compensation(df):
    """
    Use the input data frame containing the year in company, year in industry and compensation. Plot the scatter plots
    for year in industry vs compensation and year in company vs compensation. Create linear regression for both
    scatter plots.
    :param df: data frame containing the year in company, year in industry and compensation
    :return: None
    """
    # sample 10% of the data o that the plot doesn't seem too crowd.
    df = df.sample(frac=.1, random_state=9)

    # these are pd.Series
    year_in_company = df['year_in_company']
    year_in_industry = df['year_in_industry']
    compensation = df['compensation']

    # new plot
    fig = plt.figure()
    ax = plt.gca()

    # plot the year in industry vs compensation and company vs compensation
    ax.scatter(x=year_in_industry, y=compensation, s=7, label="Year in Industry", color='#0343DF')
    ax.scatter(x=year_in_company, y=compensation, s=3, label="Year in Company", color='#FAC205')

    # Note: both lines has p value significantly smaller than 1%. Thus, we are confident that years in company/industry
    # is correlated to compensation.

    # plot the regression for year in industry vs compensation
    plot_regression_line(
        x=year_in_industry,
        y=compensation,
        max_year=max(max(year_in_company), max(year_in_industry)),
        label='Year in Industry',
        color='blue'
    )

    # plot the regression for year in company vs compensation
    plot_regression_line(
        x=year_in_company,
        y=compensation,
        max_year=max(max(year_in_company), max(year_in_industry)),
        label='Year in Company',
        color='orange'
    )

    format_plot(
        fig=fig,
        ax=ax,
        xlabel='Year',
        ylabel='Compensation (k$/Year)',
        title='Observations for Year vs Compensation',
        facecolor='#D2B48C',
        textcolor='#3D1C02',
        legend_location=4
    )


def plot_position_compensation(df):
    """
    Make a box plot for different positions, experience vs compensation.
    :param df: data frame containing the position and compensation, and year in industry
    :return: None
    """
    experience = []
    yii = df['year_in_industry']
    for i in range(df.shape[0]):
        experience.append('at least 10 years' if yii[i] >= 10 else 'less than 10 years')

    df.insert(2, "experience", experience, True)

    fig = plt.figure()
    ax = sns.boxplot(
        x="positions",
        y="compensation",
        hue='experience',
        data=df,
        palette="Set3"
    )

    format_plot(
        fig=fig,
        ax=ax,
        xlabel='Position',
        ylabel='Compensation (k$/Year)',
        title='Box-plot: Position, Experience vs Compensation',
        facecolor='#354937',
        textcolor='#FFB0FF',
        legend_location=False
    )
    ax.set_xticklabels(ax.get_xticklabels(), rotation=15)


def plot_companies(df):
    """
    This function plots density for time_in_company for companies.
    :param df: data frame containing the companies and compensation, and year in company
    :return:
    """
    companies = df['companies']
    counter_companies = Counter(companies.tolist())

    list_companies = list(counter_companies.keys())

    # sort the companies by their correlated observations. Then, get the first 4 most frequently appeared companies.
    target_companies = tuple(sorted(list_companies, key=lambda x: counter_companies[x], reverse=True)[:4])

    # for simplicity of the plot, we only keep the first 4 companies with most observations.
    df_simplified = df[df['companies'].isin(target_companies)]

    fig = plt.figure()
    ax = sns.kdeplot(
        data=df_simplified,
        x="year_in_company",
        hue="companies",
        linewidth=2,
        shade=True,  # to make it seems a little NiuBier.
        common_norm=False,  # disable common normalization, then we have normalization for each company.
    )

    format_plot(
        fig=fig,
        ax=ax,
        xlabel='Year',
        ylabel='Probability',
        title='Years Spent in the Company',
        facecolor='#A0522D',
        textcolor='#AFFDF2',
        legend_location=False
    )

    # year starts from 0.
    plt.xlim(left=0, right=15)


def main():
    preprocess('dataScientistData.txt')
    df = pd.read_csv(filepath_or_buffer='Data Scientist data.csv', index_col=[0])
    plot_yii_compensation(df[['year_in_company', 'year_in_industry', 'compensation']])
    plt.show(block=False)
    plot_position_compensation(df[['positions', 'compensation', 'year_in_industry']])
    plt.show(block=False)
    plot_companies(df[['companies', 'compensation', 'year_in_company']])
    plt.show()


if __name__ == '__main__':
    main()
