"""
Convert the txt unformatted data into a nice DataFrame and save it to the csv file.
Authors: Kevin Lian, Jack Zhang
"""
import pandas as pd


def categerize(line):
    """
    Data cleaning: the original data is not well-formatted. For example, there are categories: ML, AI, Machine Learning,
    machine learning, Computer Vision, NLP, natural language processing, ML/AI,  ML / AI, etc. They all just belong to
    Machine Learning category. This function will convert the unformatted category into well-formatted categories.
    :param line: string
    :return: string for category
    """
    position = line.split(',')[0].upper()
    if any([x in position for x in ('ML', 'AI', 'LEARNING', 'NLP', 'VISION', 'LANGUAGE')]):
        position = 'MACHINE LEARNING'
    elif 'PRODUCT' in position:
        position = 'PRODUCT'
    elif 'MARKET' in position:
        position = 'MARKET'
    elif any([x in position for x in ('DATA', 'ANALYTICS', 'ANALYST', 'DS', 'GENERAL')]):
        position = 'GENERAL ANALYTICS'
    else:
        position = 'OTHERS'

    # capitalization
    position = " ".join([x.capitalize() for x in position.split()])
    return position


def categerize_Google(line):
    """
    This is a similar function with categerize(). It is designed for SDE instead of Data Scientist. SDE has different
    fields of study. Thus, the processing is different.
    :param line: string
    :return: string for category
    """
    position = line.split(',')[0].upper()
    if any([x in position for x in ('ML', 'AI', 'LEARNING', 'NLP', 'VISION', 'LANGUAGE')]):
        position = 'MACHINE LEARNING'
    elif any([x in position for x in ('MOBILE', 'ANDROID', 'IOS')]):
        position = 'MOBILE'
    elif 'DISTRIBUTED' in position:
        position = 'DISTRIBUTED SYSTEM'
    elif 'API' in position:
        position = 'API Development'
    elif any([x in position for x in ('WEB', 'FRONTEND', 'BACKEND', 'STACK')]):
        position = 'WEB'
    # else:
    #     position = 'OTHERS'

    # capitalization
    position = " ".join([x.capitalize() for x in position.split()])
    return position


def find_level(line):
    level = line.strip().replace('L', '').replace('l', '')

    get_level = {
        'Staff SWE': 6,
        'Apprentice': 2,
        'E3': 3,
        '3 software engineer': 3,
        'SWE II': 3,
        'Manager (6)': 6,
        'Senior SWE': 5,
        'Intern': 2
    }

    if not level.isnumeric():
        level = get_level[level]
    else:
        level = int(level)

    return level


def preprocess(fp, saved_file='Data Scientist data.csv'):
    """
    Read the txt file (not really well-formatted). parse the data, and build a DataFrame. Then, save the data into a csv
    file.
    :param fp: file path
    :return: DataFrame that is the same as what saved in the csv file.
    """
    companies = []
    locations = []
    positions = []
    levels = []
    year_in_company = []
    year_in_industry = []
    compensation = []

    with open(fp) as f:
        lines = f.readlines()
        for counter, line in enumerate(lines):
            mod = counter % 10  # modulo 10
            line = line.strip()
            # the txt file are formatted like below. Thus, using the modulo trick helps us to extract the data
            # perfectly:
            #
            # 0: Facebook
            # 1: Seattle, WA | 1 / 11 / 20
            # 2:
            # 3: IC5
            # 4: Salary
            # 5:
            # 6: 1 / 10
            # 7: $230, 000
            # 8: 165k | 40k | 25
            # 9:
            # ... next observation
            if mod == 0:
                companies.append(line)
            elif mod == 1:
                location = line.split(' | ')[0][-2:]
                locations.append(location)
            elif mod == 3:
                if fp == 'GoogleSDEData.txt':
                    levels.append(find_level(line))
            elif mod == 4:
                if fp == 'GoogleSDEData.txt':
                    positions.append(categerize_Google(line))
                else:
                    positions.append(categerize(line))
            elif mod == 6:
                yic, yii = line.split(' / ')
                # decimal year
                if '.' in yic:
                    yic = yic.split('.')[0]
                if '.' in yii:
                    yii = yii.split('.')[0]
                year_in_company.append(int(yic))
                year_in_industry.append(int(yii))
            elif mod == 7:
                compensation.append(int(line[1:].replace(',', '').replace('k', '000').replace('m', '000000')) / 1000)

    df = pd.DataFrame()

    if fp == 'GoogleSDEData.txt':
        df = pd.DataFrame(data={
            'companies': companies,
            'locations': locations,
            'levels': levels,
            'positions': positions,
            'year_in_company': year_in_company,
            'year_in_industry': year_in_industry,
            'compensation': compensation
        })

    else:
        df = pd.DataFrame(data={
            'companies': companies,
            'locations': locations,
            'positions': positions,
            'year_in_company': year_in_company,
            'year_in_industry': year_in_industry,
            'compensation': compensation
        })

    df.to_csv(saved_file)
    return df


def main():
    # preprocess('dataScientistData.txt')
    preprocess('GoogleSDEData.txt', 'GoogleSDE.csv')


if __name__ == '__main__':
    main()
