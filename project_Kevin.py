"""
This python program will plot below figures:
box plot: years spent in company / years spent in industry vs level
scatter + regression: level vs compensation for different fields
histogram: compensation vs location.

Author: Kevin Lian
"""

from collections import Counter

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from Plotting_Assistant import plot_regression_line, format_plot

from data_preprocessing import preprocess


def plot_year_vs_level(df):
    """
    box plot for years spent in industry vs level they got from Google
    :param df: DataFrame containing years in industry and level
    :return: None
    """
    fig = plt.figure()
    ax = sns.boxplot(
        x="levels",
        y="year_in_industry",
        data=df,
        palette="Set3"
    )

    format_plot(
        fig=fig,
        ax=ax,
        xlabel='Level at Google (max 10)',
        ylabel='Year in Industry',
        title='Box-plot: Years in Industry vs Levels',
        facecolor='#B4B3D0',
        textcolor='#CB4C2F',
        legend_location=False
    )


def plot_compensation_vs_level(df):
    """
    Plot the scatter plot and linear regression for compensation vs level for different positions.
    :param df: DataFrame containing compensation, level, and positions
    :return: None
    """
    Distributed_System = df[df["positions"] == 'Distributed System']
    Web = df[df["positions"] == 'Web']
    API = df[df["positions"] == 'Api Development']
    Machine_Learning = df[df["positions"] == 'Machine Learning']

    # new plot
    fig = plt.figure()
    ax = plt.gca()

    colors = ['red', 'blue', 'purple', '#04D8B2']
    labels = ['Distributed System', 'Web', 'API', 'Machine Learning']
    for i, df_subset in enumerate([Distributed_System, Web, API, Machine_Learning]):
        if df_subset.shape[0] > 20:
            df_subset = df_subset.sample(frac=20 / df_subset.shape[0], random_state=3)

        compensation = df_subset['compensation']
        levels = df_subset['levels']
        color = colors[i]
        label = labels[i]

        # plot the year in levels vs compensation
        ax.scatter(x=levels, y=compensation, s=3, label=label, color=color)

        # plot the regression for year in industry vs compensation
        plot_regression_line(
            x=levels,
            y=compensation,
            max_year=8,
            label=label,
            color=color
        )

    format_plot(
        fig=fig,
        ax=ax,
        xlabel='Level at Google (max 10)',
        ylabel='Compensation (k$/Year)',
        title='Compensation vs Levels for Different Fields',
        facecolor='#E7F5E1',
        textcolor='#380A1E',
        legend_location=2
    )


def plot_level_vs_location(df):
    """
    Plot the histogram plot for level for different locations.
    :param df: DataFrame containing locations and levels
    :return: None
    """
    # We only consider 3 locations. Otherwise, the plot will be a big mess.
    df = df[df['locations'].isin(['CA', 'WA', 'NY'])]

    # new plot
    fig = plt.figure()
    ax = sns.histplot(
        data=df,
        x="levels",
        hue="locations",
        linewidth=2,
        bins=8,
        # discrete=True,
        element="poly"
    )

    format_plot(
        fig=fig,
        ax=ax,
        xlabel='Level at Google (max 10)',
        ylabel='Occurrence',
        title='Levels Occurrence in Different Location',
        facecolor='#DDEEF0',
        textcolor='#42110F',
        legend_location=False
    )


def main():
    preprocess('GoogleSDEData.txt', 'GoogleSDE.csv')
    df = pd.read_csv(filepath_or_buffer='GoogleSDE.csv', index_col=[0])
    plot_year_vs_level(df[['year_in_industry', 'levels']])
    plt.show(block=False)
    plot_compensation_vs_level(df[['compensation', 'levels', 'positions']])
    plt.show(block=False)
    plot_level_vs_location(df[['levels', 'locations']])
    plt.show()


if __name__ == '__main__':
    main()
