"""
This module contains handy helper method for plotting.
Author: Kevin Lian, Jack Zhang
"""
import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import linregress


def plot_regression_line(x, y, max_year, label, color):
    """
    Fit the regression model and plot the line on the figure. The parameters are self-explanatory. We also print whether
    we are confident about the correlation into the console.
    :return: None
    """
    # y = ax + b
    model = linregress(x, y)
    b, a = model.intercept, model.slope
    print("p value is {} for {}. We are {} about the correlation.".format(
        model.pvalue, label, 'confident' if model.pvalue < 0.05 else 'not confident'))

    if max_year < 10:
        years = range(1, max_year + 1)
    else:
        years = range(max_year + 1)
    prediction = [a * x + b for x in years]
    pd.Series(data=prediction, index=years).plot(label='regression: ' + label, color=color)


def format_plot(fig, ax, xlabel, ylabel, title, facecolor, textcolor, legend_location, ax_facecolor='#A0A0A0'):
    """
    This function is used for formatting the plots (since each plot needs a formatting, I grouped the code together).
    parameters are self-explanatory.
    :return: None
    """
    # outer (fig) and inner (ax) board colors
    fig.patch.set_facecolor(facecolor)
    ax.set_facecolor(ax_facecolor)

    # spine colors
    ax.spines['top'].set_color(textcolor)
    ax.spines['bottom'].set_color(textcolor)
    ax.spines['left'].set_color(textcolor)
    ax.spines['right'].set_color(textcolor)

    # set the x and y tick color
    ax.tick_params(colors=textcolor, labelsize=12)

    # set the label color
    plt.xlabel(xlabel, fontsize=16, color=textcolor)
    plt.ylabel(ylabel, fontsize=16, color=textcolor)

    # set the title color
    plt.title(title, fontsize=16, color=textcolor)

    if not (legend_location is False):
        plt.legend(fontsize=10, loc=legend_location)

    plt.tight_layout()
